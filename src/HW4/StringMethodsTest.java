import org.junit.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * A test class for StringMethods.
 */
public class StringMethodsTest {

    private NCString empty, a, aa, aba, abba, ab, aabb, A, Aa,  AA, ABA, AABB, ABBA, aba_aba, nathan, aab, baa;

    @Before
    public void setupFixtures(){
        empty = new NCString("");
        a = new NCString("a");
        aa = new NCString("aa");
        aba = new NCString("aba");
        aabb = new NCString("aabb");
        abba = new NCString("abba");
        Aa = new NCString("Aa");
        ab = new NCString("ab");
        A = new NCString("A");
        AA = new NCString("AA");
        ABA = new NCString("ABA");
        AABB = new NCString("AABB");
        ABBA = new NCString("ABBA");
        aba_aba = new NCString("aba aba");
        nathan = new NCString("nathan");
        aab = new NCString("aab");
        baa = new NCString("baa");
    }

    /**
     * Tests whether the areEquals() method works.
     */
    @Test
    public void testAreEquals(){
        // ARE EQUAL
        assertTrue(StringMethods.areEqual(empty,empty));
        assertTrue(StringMethods.areEqual(a,new NCString("a")));
        assertTrue(StringMethods.areEqual(aa,new NCString("aa")));
        assertTrue(StringMethods.areEqual(aba,new NCString("aba")));
        assertTrue(StringMethods.areEqual(aabb,new NCString("aabb")));
        assertTrue(StringMethods.areEqual(nathan,new NCString("nathan")));
        assertTrue(StringMethods.areEqual(aab,new NCString("aab")));
        assertTrue(StringMethods.areEqual(baa,new NCString("baa")));

        // ARE NOT EQUAL
        assertFalse(StringMethods.areEqual(empty,a));
        assertFalse(StringMethods.areEqual(empty,A));
        assertFalse(StringMethods.areEqual(a,A));
        assertFalse(StringMethods.areEqual(a,aa));
        assertFalse(StringMethods.areEqual(aa,Aa));
        assertFalse(StringMethods.areEqual(Aa,aa));
        assertFalse(StringMethods.areEqual(a,empty));
        assertFalse(StringMethods.areEqual(empty,a));
        assertFalse(StringMethods.areEqual(aabb,AABB));
        assertFalse(StringMethods.areEqual(aba,aa));
        assertFalse(StringMethods.areEqual(aab,baa));
        assertFalse(StringMethods.areEqual(baa,aab));
    }

    /**
     * Tests whether the isPalindrome() method works.
     */
    @Test
    public void testIsPalindrome(){
        // ARE PALINDROMES
        assertTrue(StringMethods.isPalindrome(empty));
        assertTrue(StringMethods.isPalindrome(a));
        assertTrue(StringMethods.isPalindrome(aa));
        assertTrue(StringMethods.isPalindrome(aba));
        assertTrue(StringMethods.isPalindrome(AA));
        assertTrue(StringMethods.isPalindrome(abba));
        assertTrue(StringMethods.isPalindrome(ABA));
        assertTrue(StringMethods.isPalindrome(ABBA));
        assertTrue(StringMethods.isPalindrome(aba_aba));

        // ARE NOT PALINDROMES
        assertFalse(StringMethods.isPalindrome(aabb));
        assertFalse(StringMethods.isPalindrome(ab));
        assertFalse(StringMethods.isPalindrome(nathan));
        assertFalse(StringMethods.isPalindrome(Aa));
        assertFalse(StringMethods.isPalindrome(aab));
        assertFalse(StringMethods.isPalindrome(baa));
    }
    @Test
    public void testIsEven() {
        Assert.assertTrue(StringMethods.isEven(6));
        Assert.assertTrue(StringMethods.isEven(4));
        Assert.assertTrue(StringMethods.isEven(86));
        Assert.assertTrue(StringMethods.isEven(10));
        Assert.assertTrue(StringMethods.isEven(100));
        Assert.assertTrue(StringMethods.isEven(-4));
        Assert.assertTrue(StringMethods.isEven(-20));

        Assert.assertFalse(StringMethods.isEven(89));
        Assert.assertFalse(StringMethods.isEven(21));
        Assert.assertFalse(StringMethods.isEven(37));
        Assert.assertFalse(StringMethods.isEven(-21));
        Assert.assertFalse(StringMethods.isEven(-43));
    }
    @Test
    public void testIsOdd() {
        Assert.assertTrue(StringMethods.isOdd(89));
        Assert.assertTrue(StringMethods.isOdd(21));
        Assert.assertTrue(StringMethods.isOdd(37));
        Assert.assertTrue(StringMethods.isOdd(-21));
        Assert.assertTrue(StringMethods.isOdd(-43));

        Assert.assertFalse(StringMethods.isOdd(6));
        Assert.assertFalse(StringMethods.isOdd(4));
        Assert.assertFalse(StringMethods.isOdd(86));
        Assert.assertFalse(StringMethods.isOdd(10));
        Assert.assertFalse(StringMethods.isOdd(100));
        Assert.assertFalse(StringMethods.isOdd(-4));
        Assert.assertFalse(StringMethods.isOdd(-20));

    }
    @Test
    public void testMultiply() {
        Assert.assertEquals(0,StringMethods.multiply( 0,2));
        Assert.assertEquals(6,StringMethods.multiply( 3,2));
        Assert.assertEquals(286,StringMethods.multiply( 13,22));
        Assert.assertEquals(40,StringMethods.multiply( 8,5));
        Assert.assertEquals(12,StringMethods.multiply( 12,1));
        Assert.assertEquals(-12,StringMethods.multiply( -12,1));
        Assert.assertEquals(0,StringMethods.multiply( -1,0));

        Assert.assertEquals(-32,StringMethods.multiply( -4,8));
    }
    @Test
    public void testCharacterCount() {
        Assert.assertEquals(0,StringMethods.characterCount( empty, 'c'));
        Assert.assertEquals(1,StringMethods.characterCount( ab, 'a'));
        Assert.assertEquals(1,StringMethods.characterCount( aab,'b'));
        Assert.assertEquals(2,StringMethods.characterCount( aa, 'a'));
        Assert.assertEquals(3,StringMethods.characterCount( new NCString("aaa"), 'a'));
        Assert.assertEquals(10,StringMethods.characterCount( new NCString("aaaabababaaaa"), 'a'));
        Assert.assertEquals(4,StringMethods.characterCount(new NCString("test is a new i test"), 't'));

    }
}
