import java.util.NoSuchElementException;

/**
 * The main part of HW4. Implement both of these methods.
 *
 * @author  Huy Vu
 * @version 2 February 2017
 */
public class StringMethods {

    /**
     * No reason to instantiate this class, so the constructor is private.
     */
    private StringMethods() {
    }

    /*
     * TODO: Fill in the areEqual and isPalindrome methods below with recursive implementations.
     *
     * These methods are recursive, so they either solve the problem right away (the base case),
     * or reduce the input to a simpler problem before calling themselves again (the recursive case).
     */

    ///////////////////////////////////////////////////////////
    // PART ONE
    ///////////////////////////////////////////////////////////

    /**
     * Returns whether two NCStrings are equal. The comparison respects case (so 'test' != 'Test').
     *
     * @return Whether two NCStrings are equal.
     */
    public static boolean areEqual(final NCString a, final NCString b) {
        if (a.getLength() != b.getLength()) {
            return false;
        }//return false if they have different length
        if (a.getLength() == 0 && b.getLength() == 0) {
            return true;// return true if length are 0
        } else {
            try {
                if (a.getFirstChar() - b.getFirstChar() == 0 && a.getLastChar() - b.getLastChar() == 0) {
                    return areEqual(a.getMiddleChars(), b.getMiddleChars());
                } else {
                    return false;
                }//return false if there the first or last char are different

            } catch (NoSuchElementException e) {
                return true;//Return true if there is no more middle character to check
            }
        }

    }

    ///////////////////////////////////////////////////////////
    // PART TWO
    ///////////////////////////////////////////////////////////

    /**
     * Returns whether two NCStrings are palindromes. A String is a palindrome if it reads the
     * same forwards and backwards, respecting case and whitespace. E.g.:
     * <p>
     * "racecar"
     * "level"
     * "a"
     * "aa"
     * "mom"
     * "radar radar"
     * ""           <-- the empty string.
     * <p>
     * The following would not be considered palindromes:
     * <p>
     * "example"
     * "top spot"   <-- because of the space
     * "Mom"        <-- because of the capitalization
     *
     * @param s The NCString to examine. Can be assumed to be non-null.
     * @return Whether the argument is a palindrome, as defined above.
     */
    public static boolean isPalindrome(final NCString s) {
        if (s.getLength() == 0) {
            return true;
        }// return true if the string is empty
        if (s.getLength() == 1) {
            return true;
        }// return true if the string has just one element
        else {
            try {
                if (s.getFirstChar() - s.getLastChar() == 0) {
                    return isPalindrome(s.getMiddleChars());
                } else {
                    return false;
                }//return false if there the first or last char are different

            } catch (NoSuchElementException e) {
                return true;//Return true if there is no more middle character to check
            }
        }
    }

    public static boolean isEven(final int i) {
        int newI;
        if (i < 0) {
            newI = -i;
        } else {
            newI = i;
        }//works for negative and positive
        if (newI == 0) {
            return false;
        } else if (newI == 1) {
            return false;
        } else if (newI == 2) {
            return true;
        } else {
            return isEven(newI - 2);
        }
    }

    public static boolean isOdd(int i) {
        int newI;
        if (i < 0) {
            newI = -i;
        } else {
            newI = i;
        }//works for negative and positive
        if (newI == 0) {
            return false;
        } else if (newI == 1) {
            return true;
        } else if (newI == 2) {
            return false;
        } else {
            return isOdd(newI - 2);
        }
    }

    public static int multiply(int a, int b) {
        if (a == 0 || b == 0) {
            return 0;
        } else if (a == 1) {
            return b;
        } else if (b == 1) {
            return a;
        } else {
            return a + multiply(a, b - 1);
        }
    }
    private static boolean isMiddleTheSame =false;
    public static int characterCount(NCString s, final char c) {
        if (s.getLength() == 0) {
            return 0;
        } else {
            try {
                if (s.getFirstChar() == c && s.getLastChar() == c) {
                    isMiddleTheSame =true; //setSwitch to 1;
                    return characterCount(s.getMiddleChars(), c) + 2;//add two if the first and last char are the same to c
                } else if (s.getFirstChar() == c ||  s.getLastChar() == c) {
                    return characterCount(s.getMiddleChars(), c) + 1;//add one if the first or last char is the same to c
                } else {
                    isMiddleTheSame =false;
                    return characterCount(s.getMiddleChars(), c);
                    //nothing is added if the both first or last char are not the same as c
                }
            } catch (NoSuchElementException e) {
                if(isMiddleTheSame){
                    return 1;//return one if the middle number is the same like c
                }  else{
                    return 0;//return one if the middle number is not the same like c
                }

            }
        }

    }
}
