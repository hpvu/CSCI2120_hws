/**
 * A class to test ComplexNumber
 * and RealNumber models
 *
 *
 * @author 	Huy Vu
 * @version 20 January 2017
 */
public class TestNum{
    public static void main(String[] args){
        System.out.println("Testing ComplexNumber class");

        ComplexNumber a = new ComplexNumber(4,5);
        ComplexNumber b = new ComplexNumber(2,6);
        System.out.printf("(%s) + (%s) = %s\n", a, b, a.add(b));
        System.out.printf("(%s) - (%s) = %s\n", a, b, a.sub(b));
        System.out.printf("(%s) * (%s) = %s\n", a, b, a.mul(b));
        System.out.printf("(%s) / (%s) = %s\n", a, b, a.div(b));
        System.out.println("Dividing by zero");
        try{
            b = new ComplexNumber(0,0);
            System.out.printf("(%s) / (%s) = %s\n", a, b, a.div(b));
        }
        catch(ArithmeticException e){
            e.printStackTrace();
        }

        System.out.println("\nTesting RealNumber class");

        RealNumber c = new RealNumber(3);
        RealNumber d = new RealNumber(6);

        System.out.printf("(%s) + (%s) = %s\n", c, d, c.add(d));
        System.out.printf("(%s) - (%s) = %s\n", c, d, c.sub(d));
        System.out.printf("(%s) * (%s) = %s\n", c, d, c.mul(d));
        System.out.printf("(%s) / (%s) = %s\n", c, d, c.div(d));
        System.out.println("Dividing by zero");
        try{
            d = new RealNumber(0);
            System.out.printf("(%s) / (%s) = %s\n", c, d, c.div(d));
        }
        catch(ArithmeticException e){
            e.printStackTrace();
            d = new RealNumber(6);
        }

        System.out.printf("\nIs %s is less than %s: %b\n", c , d, c.isLessThan(d));
        System.out.printf("Is %s is greater than %s: %b\n", c , d, c.isGreaterThan(d));
    }
}