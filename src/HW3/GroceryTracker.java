import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * A class that create a program that use the GroceryList
 *
 *  @author 	Huy Vu
 * @version 21 January 2017
 */

public class GroceryTracker {

    private static GroceryList record = new GroceryList();
    private static Scanner kbd = new Scanner(System.in);
    private static boolean isSave = true;
    private static String pathName ="";
    public static void main(String[] args){

        for(String i : args){
            pathName = i;
        }

        Path path = Paths.get(pathName);
        if(Files.exists(path)){
             readObjectFromFile(pathName);
        } else{
            writeObjectToFile(record, pathName);
        }
        while(menu(pathName));

    }

    private static boolean menu(String pathName) {
        System.out.println("Welcome to GroceryTracker!\n" +
                "Current File: " + pathName +
                "\nPlease select an option:\n" +
                "\t1) View List\n" +
                "\t2) Add An Item\n" +
                "\t3) Remove An Item\n" +
                "\t4) Save\n" +
                "\t5) Quit\n" +
                "Choice: _");
        String choice = kbd.next();
        if (choice.equals("1")) {
            System.out.println(record);
            System.out.println();
        } else if (choice.equals("2")) {
            addAnItem();
        } else if (choice.equals("3")) {
            removeItemInList();
        } else if (choice.equals("4")) {
            saveItem();
        } else if(choice.equals("5")){
            if(!isSave){
                Scanner kbd = new Scanner(System.in);
                while(true) {
                    System.out.println("Your items is not save yet.\nSave: y or n\n");
                    String response = kbd.next();
                    if (response.equalsIgnoreCase("y")) {
                        saveItem();
                        return false;
                    } else if (response.equalsIgnoreCase("n")) {
                        return false;
                    } else {
                        System.out.println("Please enter y or n");

                    }
                }
            }
            return false;
        }
        else{
            System.out.println("Please enter a valid choice");
        }
        return true;
    }

    private static void saveItem(){
        System.out.println("Saving the list to file ...");
        writeObjectToFile(record, pathName);
        isSave = true;
    }
    private static void addAnItem(){
        Scanner kbd = new Scanner(System.in);
        System.out.println("Enter item to add");
        String item = kbd.nextLine();
        record.addItem(item);
        isSave = false;
    }
    private static void removeItemInList(){
        Scanner kbd = new Scanner(System.in);
        System.out.println("Enter the index of the item to remove");

        while (true) {
            try {
                while (true) {
                    int index = kbd.nextInt();
                    if (record.getItemCount() == 0) {
                        System.out.println("There is no item to remove\n");
                        break;
                    }
                    if (index > 0 && index <= record.getItemCount()) {
                        System.out.println("Removing " + record.getItemAtIndex(index - 1));
                        record.removeItemAtIndex(index - 1);
                        break;
                    } else {
                        kbd.nextLine();
                        System.out.println("Sorry please enter a valid index");
                    }
                }
                break;
            } catch (InputMismatchException e) {
                System.out.println("Sorry please enter a valid  input");
                kbd.nextLine();
            }
        }
        isSave = false;
    }


    private static void writeObjectToFile(Object obj, String fileName){

        try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName))){
                out.writeObject(obj);
            }catch(IOException e){
                e.printStackTrace();
            }
        }

    private static void readObjectFromFile( String fileName) {

            Object obj;
            try (ObjectInputStream out = new ObjectInputStream(new FileInputStream(fileName))) {
                obj = out.readObject();
                if (obj instanceof GroceryList) {
                    record = (GroceryList) obj;
                }
            } catch (ClassNotFoundException e) {
                System.out.println("Error Finding a class");
                System.exit(1);
            }catch (IOException e) {
                System.out.println("Error Reading from text file");
                System.exit(1);
            }

        }






}
