import java.io.Serializable;
import java.util.ArrayList;

/**
 * A class that models a list of grocery items.
 *
 * Hint: Use ArrayList to do all the heavy lifting for storing your items.
 *
 * @author 	Huy Vu
 * @version 21 January 2017
 */
public class GroceryList implements Serializable {
    private ArrayList<String> array;
    /**
     * Constructs a new GroceryList object that is initially empty.
     */
    public GroceryList(){
        array = new ArrayList<>();
    }

    /**
     * Returns the number of items currently in the grocery list.
     *
     * @return The number of items in the list.
     */
    public int getItemCount(){
        return array.size();
    }

    /**
     * If the given item String is not in the list yet (ignoring capitalization
     * and leading/trailing whitespace), appends the item to the end
     * of the list. Otherwise, does nothing. A GroceryList should be able
     * to hold as many unique item names as the user desires. If the item
     * contains no actual text other than whitespace, this should not be added.
     *
     * @param item The item to add.
     */
    public void addItem(final String item){

            boolean isNotInList = true;
            for(Object listItem :  array.toArray()){
                String stringItem = (String) listItem;
                if(stringItem.trim().equalsIgnoreCase(item.trim())){
                    isNotInList = false;
                    break;
                }
            }// check to see if the item is in the list
            this.addItemIsNotInList(isNotInList, item);
        }

    //private class that add the item to the array if it is not in the list yet
    private void addItemIsNotInList(boolean isNotInList, final String item){
            String newItem="";
            if(isNotInList) {
                String[] itemArray = item.split("\\s+"); //split the item into an array
                if (itemArray.length > 0) { //check to see if there is any item in list
                    if (itemArray[0].equals("")) {
                        if (item.length() > 1) { //check to see if there are more than 1 item
                            itemArray[1] = GroceryList.convertToNewForm(1, itemArray);
                            newItem = itemArray[1]; //Add the second item
                            if (itemArray.length > 2) {
                                for (int index = 2; index < itemArray.length; index++) {
                                    itemArray[index] = GroceryList.convertToNewForm(index, itemArray);
                                    newItem += " " + itemArray[index]; //Add the i item
                                }
                            }
                        }
                    } else {
                        itemArray[0] = GroceryList.convertToNewForm(0, itemArray);
                        newItem = itemArray[0];//add the first item
                        if (itemArray.length > 1) {
                            for (int index = 1; index < itemArray.length; index++) {
                                itemArray[index] = GroceryList.convertToNewForm(index, itemArray);
                                newItem += " " + itemArray[index];//add the i item
                            }
                        }
                    }
                    array.add(newItem);
                }//add the item if it is in the list
            }
        }

    //convert the item to new form (eg: eGG => Egg)
    private static String convertToNewForm(int index, String[] itemArray){
        return itemArray[index].substring(0, 1).toUpperCase() + itemArray[index].substring(1).toLowerCase();
    }

    /**
     * Removes the item at the specified index. If the index specified is
     * >= this.getItemCount(), an IllegalArgumentException should be thrown.
     * After removal of an item, the index of all items past the specified index
     * should be decremented. E.g.:
     *
     * Before:
     * 0 => Eggs
     * 1 => Milk
     * 2 => Spinach
     *
     * list.removeItemAtIndex(1);
     *
     * After:
     * 0 => Eggs
     * 1 => Spinach
     *
     * @param index The index (zero-based) to remove.
     */
    public void removeItemAtIndex(final int index) throws IllegalArgumentException{
        if(index >= this.getItemCount()){
            throw new IllegalArgumentException("Should not be able to remove an item from an empty list.");
        }
        array.remove(index);
    }

    /**
     * Returns the item String at the specified index. If the index specified
     * is >= this.getItemCount(), an IllegalArgumentException should be thrown. The
     * String returned should be given in "canonical" form", that is, with no leading/trailing
     * whitespace and the first letter of each individual word capitalized, regardless of
     * how the item was added initially. E.g.:
     *
     * "eggs" => "Eggs"
     * "toilet paper" => "Toilet Paper"
     * "MILK" => "Milk"
     * "  coffee " => "Coffee"
     *
     * @param index The index (zero-based) to fetch.
     * @return The grocery item text at the given index, in the canonical form specified above.
     */
    public String getItemAtIndex(final int index) throws IllegalArgumentException{
        if(index >= this.getItemCount()){
            throw new IllegalArgumentException("Should not be able to get an item from an empty list.");
        }
        return array.get(index);

    }

    /**
     * @{inheritDoc}
     *
     * Returns a String representation of this list. Should use the StringBuilder class to build
     * up the result. A representation of a GroceryList is a human-readable series of lines with
     * a line number (1-based), followed by a period and space (". "), followed by the item. There
     * should be no trailing newline. If the list is empty, the words "(Empty List)" should be returned.
     *
     * E.g. for GroceryList {0 => "Eggs", 1 => "Bacon", 2 => "Bread"}, it should return:
     *
     * "1. Eggs
     * 2. Bacon
     * 3. Bread"
     */
    @Override
    public String toString(){
        if(this.getItemCount()==0) return "(Empty List)";
        String listString = "";
        if(this.getItemCount()>0) listString += "1. " + array.get(0);
        for(int i=1; i<this.getItemCount();i++){
            listString+= "\n" +(i+1) +". " +array.get(i);
        }
        return listString;
    }

}
