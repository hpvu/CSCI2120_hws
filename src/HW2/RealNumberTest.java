import org.junit.*;
import static org.junit.Assert.*;

/**
 * Test class for class <code>RealNumber</code>.
 *
 * PLEASE COMPLETE STUBBED METHODS AND INCOMPLETE
 * DOCUMENTATION.
 *
 * @author	Huy VU
 * @version	31 January 2017
 */
public class RealNumberTest {
    private RealNumber real1;
    private RealNumber real2;
    private RealNumber real3;
    private RealNumber real4; 	// has a numerator of zero
    private double delta =   0.001;
    /**
     * Creates test fixtures before each test is run.
     */
    @Before
    public void setUp() {
        real1 = new RealNumber(1);
        real2 = new RealNumber(7);
        real3 = new RealNumber( 5.3);
        real4 = new RealNumber(0);
    } // end method setUp

    /**
     * Tests the <code>equals</code> method of <code>RealNumber</code>.
     * Recall the contract for equals. The tests should ensure that
     * all aspects of that contract are fulfilled.
     */
    @Test
    public void testEquals() {
        RealNumber nullCom = null;
        assertFalse(real4.equals(nullCom)); 		// null vs non-null references
        assertFalse(real1.equals(real2));
        assertTrue(real3.equals(real3)); 			// reflexivity
        assertEquals(new RealNumber(7), real2);
        assertEquals(real2, new RealNumber(7)); 	// symmetry
        RealNumber transitiveTestReal1 = new RealNumber(1);
        RealNumber transitiveTestReal2 = new RealNumber( 1);
        assertEquals(real1, transitiveTestReal1);
        assertEquals(transitiveTestReal1, transitiveTestReal2);
        assertEquals(real1, transitiveTestReal2);	// transitivity
        assertFalse(real4.equals(new RealNumber(1)));
        assertTrue(real4.equals(new RealNumber(0)));
        assertTrue(real1.equals(new RealNumber(1)));
        assertEquals(new RealNumber(  5.3), real3);

    } // end method testEquals


    /**
     * Tests the <code>toString</code> method of <code>RealNumber</code>.
     */
    @Test
    public void testToString() {
        assertEquals("1.00", real1.toString());
        assertEquals("12.00",new RealNumber(12).toString());

		/* Stubbed */
    } // end method testToString

    /**
     * Tests the <code>add</code> method of <code>RealNumber</code>.
     * This test utilizes a few different methods of comparing values,
     * including using the other, already tested methods of
     * <code>ComplexNumber</code>Number</code>.
     */
    @Test
    public void testAdd() {
        assertEquals("8.00", real1.add(real2).toString());
        assertEquals(new RealNumber( 12.3), real2.add(real3));
        assertEquals(7, real4.add(real2).getReal(), delta);
        assertEquals("6.30", real3.add(real1).toString());
        assertEquals(new RealNumber(-2), real1.add(new RealNumber(-3)));
    } // end method testAdd

    /**
     * Tests the <code>sub</code> method of <code>RealNumber</code>.
     * This test utilizes a few different methods of comparing values,
     * including using the other, already tested methods of
     * <code>ComplexNumber</code>.
     */
    @Test
    public void testSub() {
        assertEquals("-6.00", real1.sub(real2).toString());
        assertEquals(new RealNumber(  1.7).getReal(), real2.sub(real3).getReal(), delta);
        assertEquals(-7, real4.sub(real2).getReal(),delta);
        assertEquals("4.30", real3.sub(real1).toString());
        assertEquals(new RealNumber(4), real1.sub(new RealNumber(-3)));
		/* Stubbed */
    } // end method testSub

    /**
     * Tests the <code>mul</code> method of <code>RealNumber</code>.
     * This test utilizes a few different methods of comparing values,
     * including using the other, already tested methods of
     * <code>ComplexNumber</code>.
     */
    @Test
    public void testMul() {
        assertEquals("7.00", real1.mul(real2).toString());
        assertEquals(new RealNumber(  37.10 ), real2.mul(real3));
        assertEquals(0, real4.mul(real2).getReal(), delta);
        assertEquals("5.30", real3.mul(real1).toString());
        assertEquals(new RealNumber(-3), real1.mul(new RealNumber(-3)));
		/* Stubbed */
    } // end method testMul

    /**
     * Tests <code>div</code> method of <code>RealNumber</code> and
     * ensures that exception is being thrown appropriately.
     */
    @Test
    public void testDiv() {
        assertEquals("0.14", real1.div(real2).toString());
        assertEquals(new RealNumber(1.32).getReal(), real2.div(real3).getReal(), delta);
        assertEquals(0.0, real4.div(real2).getReal(), delta);
        assertEquals("5.30", real3.div(real1).toString());
        assertEquals(  -0.333, real1.div(new RealNumber(-3)).getReal(),delta);
        assertEquals(0.0, real1.div(new RealNumber(-3)).getImagine(),delta);
        //test that proper message is thrown when exception is thrown
        try{
            real3.div(new RealNumber(0));
        }
        catch(ArithmeticException e){
            assertEquals("Division by zero is undefined", e.getMessage());
        }
		/* Stubbed */
    } // end method testDiv


    /**
     * Tests <code>isLessThan</code> method of <code>RealNumber</code> and
     * ensures that exception is being thrown appropriately.
     */
    @Test
    public void testIsLessThan() {
        assertTrue(real1.isLessThan(real3));
        assertFalse(real3.isLessThan(new RealNumber(-13.4)));
    }

    /**
     * Tests <code>isGreaterThan</code> method of <code>RealNumber</code> and
     * ensures that exception is being thrown appropriately.
     */
    @Test
    public void testIsGreaterThan() {
        assertFalse(real1.isGreaterThan(real3));
        assertTrue(real3.isGreaterThan(new RealNumber(-0.4)));
    }

}

