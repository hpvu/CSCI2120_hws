/**
 * A class to model complex numbers
 * and some of their main operations.
 * Methods include add, sub,
 * mul, div, getters, equals,
 * toString
 *
 * @author 	Huy Vu
 * @version 21 January 2017
 */

public class ComplexNumber{
    private final double real;
    private final double img;


    /**
     * Initializes real and imaginary parts to values passed
     * as arguments.
     *
     * @param	real	the complex number's real part
     * @param	img		the complex number's imaginary part
     */
    public ComplexNumber(double real, double img){
        this.real= real;
        this.img= img;
    }

    /**
     * Returns the real part of this ComplexNumber instance.
     *
     * @return	this.real
     */
    public double getReal(){
        return this.real;

    }

    /**
     * Returns the imaginary part of this ComplexNumber instance.
     *
     * @return	this.img
     */
    public double getImagine(){
        return this.img;
    }

    /**
     * Return a true if the number exits
     * and equals to other ComplexNumber
     *
     * @param other the other ComplexNumber number for comparing
     */
    @Override
    public boolean equals(Object other){
        if(other !=null && other instanceof ComplexNumber){
            ComplexNumber num  = (ComplexNumber) other;
            return (this.getReal()== num.getReal() && this.getImagine() == num.getImagine());
        }
        return false;
    }

    /**
     * Returns a String representation of this ComplexNumber
     * instance.
     *
     * @return	a String representation of this ComplexNumber
     */
    @Override
    public String toString(){
        return String.format("%.2f%2s%.2fi", this.real,
                (this.img>=0)? " + ": " - ",
                (this.img>=0)?this.img:(-this.img));
    }
    /**
     * Adds this complex number to another provided
     * as an argument. Returns the sum as a new
     * ComplexNumber instance.
     *
     * @param	other	the other added ComplexNumber
     * @return	a new ComplexNumber that is the sum
     */
    public ComplexNumber add(ComplexNumber other){
        double newA = this.real + other.getReal();
        double newB= this.img + other.getImagine();
        return new ComplexNumber(newA, newB);
    }

    /**
     * Subtracts this complex number from another provided
     * as an argument. Returns the difference as a new
     * ComplexNumber instance.
     *
     * @param	other	the other Fraction used to subtract
     * @return	a new ComplexNumber that is the difference
     */
    public ComplexNumber sub(ComplexNumber other){
        double newA = this.real - other.getReal();
        double newB= this.img - other.getImagine();
        return new ComplexNumber(newA, newB);
    }

    /**
     *  Multiply this complex number by other
     * ComplexNumber provided as an argument.
     * Returns the quotient
     * as a new ComplexNumber instance.
     *
     * @param	other	the multiplier ComplexNumber
     * @return	a new ComplexNumber that is the product
     */
    public ComplexNumber mul(ComplexNumber other){
        double newA = this.real * other.getReal() - this.img * other.getImagine();
        double newB= this.img * other.getReal() + this.real * other.getImagine();
        return new ComplexNumber(newA, newB);
    }

    /**
     * Divides this complex number by divisor provided
     * as an argument. Returns the quotient as a
     * new ComplexNumber instance.
     *
     * @param	other	the divisor ComplexNumber
     * @throws	ArithmeticException throws exception if the denominator is 0
     * @return	a new ComplexNumber that is the quotient
     */
    public ComplexNumber div(ComplexNumber other) throws ArithmeticException{
        double denominator = (Math.pow(other.getReal(), 2) +   Math.pow(other.getImagine(), 2));
        if(denominator == 0){
            throw new ArithmeticException("Division by zero is undefined");
        }
        double numeratorA = this.real * other.getReal() + this.img * other.getImagine();
        double numeratorB =  this.img * other.getReal() - this.real * other.getImagine();
        double newA =  numeratorA / denominator ;
        double newB=     numeratorB / denominator ;
        return new ComplexNumber(newA, newB);
    }
}