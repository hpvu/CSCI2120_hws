import org.junit.*;
import static org.junit.Assert.*;

/**
 * Test class for class <code>ComplexNumber</code>.
 *
 * PLEASE COMPLETE STUBBED METHODS AND INCOMPLETE
 * DOCUMENTATION.
 *
 * @author	Huy VU
 * @version	31 January 2017
 */
public class ComplexNumberTest {
    private ComplexNumber com1;
    private ComplexNumber com2;
    private ComplexNumber com3;
    private ComplexNumber com4; 	// has a numerator of zero
    private double delta =   0.001;
    /**
     * Creates test fixtures before each test is run.
     */
    @Before
    public void setUp() {
        com1 = new ComplexNumber(1, 2);
        com2 = new ComplexNumber(7, 3);
        com3 = new ComplexNumber(5, -5);
        com4 = new ComplexNumber(0, 4);
    } // end method setUp

    /**
     * Tests the <code>getReal</code> method of <code>ComplexNumber</code>.
     */
    @Test
    public void testGetReal() {
        assertEquals(1, com1.getReal(), delta);
        assertEquals(7, com2.getReal(), delta);
        assertEquals(5, com3.getReal(), delta);
        assertEquals(0, com4.getReal(), delta);
    }

    /**
     * Tests the <code>equals</code> method of <code>ComplexNumber</code>.
     * Recall the contract for equals. The tests should ensure that
     * all aspects of that contract are fulfilled.
     */
    @Test
    public void testEquals() {
        String word = "Hello";
        ComplexNumber nullCom = null;
        assertFalse(com4.equals(nullCom)); 		// null vs non-null references
        assertFalse(com1.equals(com2));
        assertTrue(com3.equals(com3)); 			// reflexivity
        assertEquals(new ComplexNumber(7, 3), com2);
        assertEquals(com2, new ComplexNumber(7, 3)); 	// symmetry
        ComplexNumber transitiveTestCom1 = new ComplexNumber(1, 2);
        ComplexNumber transitiveTestCom2 = new ComplexNumber( 1, 2);
        assertEquals(com1, transitiveTestCom1);
        assertEquals(transitiveTestCom1, transitiveTestCom2);
        assertEquals(com1, transitiveTestCom2);	// transitivity
        assertFalse(com4.equals(new ComplexNumber(1, 4)));
        assertTrue(com4.equals(new ComplexNumber(0, 4)));
        assertTrue(com1.equals(new ComplexNumber(1, 2)));
        assertEquals(new ComplexNumber(5, -5), com3);

    } // end method testEquals

    /**
     * Tests the <code>toString</code> method of <code>ComplexNumber</code>.
     */
    @Test
    public void testToString() {
        assertEquals("1.00 + 2.00i", com1.toString());
        assertEquals("12.00 - 2.00i",new ComplexNumber(12, -2).toString());

		/* Stubbed */
    } // end method testToString

    /**
     * Tests the <code>add</code> method of <code>ComplexNumber</code>.
     * This test utilizes a few different methods of comparing values
     */
    @Test
    public void testAdd() {
        assertEquals("8.00 + 5.00i", com1.add(com2).toString());
        assertEquals(new ComplexNumber(12, -2), com2.add(com3));
        assertEquals(7, com4.add(com2).getReal(), delta);
        assertEquals(7, com4.add(com2).getImagine(),delta);
        assertEquals("6.00 - 3.00i", com3.add(com1).toString());
        assertEquals(new ComplexNumber(-2, 4), com1.add(new ComplexNumber(-3, 2)));
    } // end method testAdd

    /**
     * Tests the <code>sub</code> method of <code>RealNumber</code>.
     * This test utilizes a few different methods of comparing values
     */
    @Test
    public void testSub() {
        assertEquals("-6.00 - 1.00i", com1.sub(com2).toString());
        assertEquals(new ComplexNumber(2, 8), com2.sub(com3));
        assertEquals(-7, com4.sub(com2).getReal(),delta);
        assertEquals(1, com4.sub(com2).getImagine(), delta);
        assertEquals("4.00 - 7.00i", com3.sub(com1).toString());
        assertEquals(new ComplexNumber(4, 0), com1.sub(new ComplexNumber(-3, 2)));
		/* Stubbed */
    } // end method testsub

    /**
     * Tests the <code>mul</code> method of <code>RealNumber</code>.
     * This test utilizes a few different methods of comparing values
     */
    @Test
    public void testMul() {
        assertEquals("1.00 + 17.00i", com1.mul(com2).toString());
        assertEquals(new ComplexNumber(50, -20), com2.mul(com3));
        assertEquals(-12, com4.mul(com2).getReal(), delta);
        assertEquals(28, com4.mul(com2).getImagine(), delta);
        assertEquals("15.00 + 5.00i", com3.mul(com1).toString());
        assertEquals(new ComplexNumber(-7, -4), com1.mul(new ComplexNumber(-3, 2)));
		/* Stubbed */
    } // end method testmul

    /**
     * Tests <code>div</code> method of <code>ComplexNumber</code> and
     * ensures that exception is being thrown appropriately.
     */
    @Test
    public void testDiv() {
        assertEquals("0.22 + 0.19i", com1.div(com2).toString());
        assertEquals(new ComplexNumber( 0.40 , 1), com2.div(com3));
        assertEquals(0.206, com4.div(com2).getReal(), delta);
        assertEquals(0.482, com4.div(com2).getImagine(), delta);
        assertEquals("-1.00 - 3.00i", com3.div(com1).toString());
        assertEquals(0.076, com1.div(new ComplexNumber(-3, 2)).getReal(),delta);
        assertEquals(-0.615, com1.div(new ComplexNumber(-3, 2)).getImagine(),delta);
        //test that proper message is thrown when exception is thrown
        try{
            com3.div(new ComplexNumber(0,0));
             		}
        catch(ArithmeticException e){
            assertEquals("Division by zero is undefined", e.getMessage());
        }
		/* Stubbed */
    } // end method testdiv
} // end class ComplexNumberTest1