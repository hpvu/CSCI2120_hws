/**
 * A class to model real numbers
 * and some of their main operations.
 * It extends real number.
 * Methods include add, sub,
 * mul, div, getters, equals,
 * toString, isLessThan, and isGreaterThan
 *
 * @author 	Huy Vu
 * @version 20 January 2017
 */
public class RealNumber extends ComplexNumber{

    /**
     * Initializes real to values passed
     * as arguments and imaginary part to 0.
     *
     * @param	real	the real number's real part
     */
    public RealNumber(double real){
        super(real, 0);
    }

    /**
     * Returns a String representation of this RealNumber
     * instance.
     *
     * @return	a String representation of this RealNumber
     */
    public String toString(){
        return String.format("%.2f", super.getReal());

    }

    /**
     * Adds this real number to another provided
     * as an argument. Returns the sum as a new
     * RealNumber instance.
     *
     * @param	other	the other added RealNumber
     * @return	a new RealNumber that is the sum
     */
    public RealNumber add(RealNumber other){
        double newA = super.getReal() + other.getReal();

        return new RealNumber(newA);
    }

    /**
     * Subtracts this real number from another provided
     * as an argument. Returns the difference as a new
     * RealNumber instance.
     *
     * @param	other	the other Fraction used to subtract
     * @return	a new RealNumber that is the difference
     */
    public RealNumber sub(RealNumber other){
        double newA = super.getReal() - other.getReal();

        return new RealNumber(newA );
    }

    /**
     *  Multiply this real number by other
     * RealNumber provided as an argument.
     * Returns the quotient
     * as a new RealNumber instance.
     *
     * @param	other	the multiplier RealNumber
     * @return	a new RealNumber that is the product
     */
    public RealNumber mul(RealNumber other){
        double newA = super.getReal() * other.getReal();
        return new RealNumber(newA);
    }

    /**
     * Divides this real number by divisor provided
     * as an argument. Returns the quotient as a
     * new RealNumber instance.
     *
     * @param	other	the divisor RealNumber
     * @throws	ArithmeticException throws exception if the denominator is 0
     * @return	a new RealNumber that is the quotient
     */
    public RealNumber div(RealNumber other) throws ArithmeticException{
        if(other.getReal() ==0 ){
            throw new ArithmeticException("Division by zero is undefined");
        }
        double newA = super.getReal() / other.getReal();
        return new RealNumber(newA);
    }


    /**
     * Return a true if other real number
     * exits and this real number is less
     * than the other real number
     *
     * @param other the other RealNumber instance for comparing
     */
    public boolean	isLessThan(RealNumber other){
        return (other != null && (super.getReal() - other.getReal() < 0))? true: false;
    }

    /**
     * Return a true if other real number
     * exits and this real number is greater
     * than the other real number
     *
     * @param other the other RealNumber instance for comparing
     */
    public boolean isGreaterThan(RealNumber other){
        return (other != null && (super.getReal() - other.getReal() > 0))? true: false;
    }
}